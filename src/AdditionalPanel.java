/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class AdditionalPanel extends JPanel {
    public final double GRIP_TAPE = 10;
    public final double BEARINGS = 30;
    public final double RISER_PADS = 2;
    public final double NUTS_BOLT_KIT = 3;

    private JCheckBox gripTape;
    private JCheckBox bearings;
    private JCheckBox riserPads;
    private JCheckBox nutsBoltsKit;


    public AdditionalPanel() {
        setLayout(new GridLayout(4, 1));

        gripTape = new JCheckBox("Grip tape");
        bearings = new JCheckBox("Bearings");
        riserPads = new JCheckBox("Riser pads");
        nutsBoltsKit = new JCheckBox("Nuts & bolts kit");

        setBorder(BorderFactory.createTitledBorder("Miscellaneous"));

        add(gripTape);
        add(bearings);
        add(riserPads);
        add(nutsBoltsKit);
    }

    public double getAddCost() {
        double addCost = 0.0;

        if (gripTape.isSelected())
            addCost += GRIP_TAPE;
        if (bearings.isSelected())
            addCost += BEARINGS;
        if (riserPads.isSelected())
            addCost += RISER_PADS;
        if (nutsBoltsKit.isSelected())
            addCost += NUTS_BOLT_KIT;

        return addCost;
    }
}