/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class WheelsPanel extends JPanel {
    public final double WHEEL_51 = 20;
    public final double WHEEL_55 = 22;
    public final double WHEEL_58 = 24;
    public final double WHEEL_61 = 28;

    private JRadioButton wheel51;
    private JRadioButton wheel55;
    private JRadioButton wheel58;
    private JRadioButton wheel61;
    private ButtonGroup bg;

    public WheelsPanel() {
        setLayout(new GridLayout(4, 1));
   
        wheel51 = new JRadioButton("51 mm", true);
        wheel55 = new JRadioButton("55 mm");
        wheel58 = new JRadioButton("58 mm");
        wheel61 = new JRadioButton("61 mm");

        bg = new ButtonGroup();
        bg.add(wheel51);
        bg.add(wheel55);
        bg.add(wheel58);
        bg.add(wheel61);

        setBorder(BorderFactory.createTitledBorder("Wheels"));

        add(wheel51);
        add(wheel55);
        add(wheel58);
        add(wheel61);
    }

    public double getWheelsCost() {
        double wheelsCost = 0.0;

        if (wheel51.isSelected())
            wheelsCost = WHEEL_51;
        else if (wheel55.isSelected())
            wheelsCost = WHEEL_55;
        else if (wheel58.isSelected())
            wheelsCost = WHEEL_58;
        else if (wheel61.isSelected())
            wheelsCost = WHEEL_61;

        return wheelsCost;
    }
}
