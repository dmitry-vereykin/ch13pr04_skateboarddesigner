/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class SkateboardDesigner extends JFrame {
    private DecksPanel decks;
    private AdditionalPanel additional;
    private JPanel downPanel;
    private AxelPanel axles;
    private WheelsPanel wheels;
    private TopBanner topBanner;
    private JPanel buttonPanel;
    private JButton calcButton;
    private final double TAX_RATE = 0.06;

    private JTextField subTotalField;
    private JTextField taxField;
    private JTextField totalField;

    public SkateboardDesigner() {
        setTitle("Skateboard designer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        topBanner = new TopBanner();
        decks = new DecksPanel();
        additional = new AdditionalPanel();
        axles = new AxelPanel();
        wheels = new WheelsPanel();
        downPanel = new JPanel();

        buildDownPanel();

        add(topBanner, BorderLayout.NORTH);
        add(decks, BorderLayout.WEST);
        add(wheels, BorderLayout.EAST);
        add(axles, BorderLayout.CENTER);
        add(downPanel, BorderLayout.SOUTH);

        pack();
        setVisible(true);
    }

    private void buildDownPanel() {
        downPanel = new JPanel();
        downPanel.setLayout(new GridLayout(1, 2));

        buildButtonPanel();

        downPanel.add(additional);
        downPanel.add(buttonPanel);
    }


    private void buildButtonPanel() {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4, 2));

        calcButton = new JButton("Calculate");
        calcButton.addActionListener(new CalcButtonListener());

        subTotalField = new JTextField(5);
        subTotalField.setHorizontalAlignment(JTextField.RIGHT);

        taxField = new JTextField(5);
        taxField.setHorizontalAlignment(JTextField.RIGHT);

        totalField = new JTextField(5);
        totalField.setHorizontalAlignment(JTextField.RIGHT);

        buttonPanel.add(new JLabel("Subtotal: "));
        buttonPanel.add(subTotalField);

        buttonPanel.add(new JLabel("Tax: "));
        buttonPanel.add(taxField);

        buttonPanel.add(new JLabel("Total: "));
        buttonPanel.add(totalField);

        buttonPanel.add(calcButton);
        buttonPanel.add(new JLabel("    Thank you  "));
    }


    private class CalcButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double subtotal, tax, total;

            subtotal = decks.getDeckCost() +
                    additional.getAddCost() +
                    axles.getAxleCost() +
                    wheels.getWheelsCost();

            tax = subtotal * TAX_RATE;
            total = subtotal + tax;

            DecimalFormat dollar = new DecimalFormat("0.00");

            subTotalField.setText(dollar.format(subtotal));
            taxField.setText(dollar.format(tax));
            totalField.setText("$" + dollar.format(total));
        }
    }

    public static void main(String[] args) {
        new SkateboardDesigner();
    }
}